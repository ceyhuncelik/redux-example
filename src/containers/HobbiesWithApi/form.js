
import React from 'react';
import { connect } from 'react-redux';
import './style.css';

import UserListItem from 'components/UserListItem';
import Input from 'components/Input';

import {
	saveHobbies,
} from 'store/actions/hobbiesActions';

class Form extends React.Component {
	state = {
		name: '',
		type: '',
	};

	userListRender = userList => userList.map(item => (<UserListItem {...item} />))

	saveHobbiesHandler = () => {
		// debugger;
		const { name, type } = this.state; 
		// this.props.dispatch(saveHobbies({ name, type}));
		this.props.saveHobbies({ name, type});
	}

	onChangeHandler = (event) => {
		debugger;
		const { value, name } = event.target;
		this.setState({ [name]: value });
	}
	render() {
		console.log('Form Render');
		return (
			<fieldset>
					<div id="legend">
						<legend className="">Add New Hobby </legend>
					</div>
					<Input onChange={this.onChangeHandler} label='Name' name='name' />
					<Input onChange={this.onChangeHandler} label='Type' name='type' />
					<div className="control-group">
						<div className="controls">
							<button onClick={() => this.saveHobbiesHandler() } className="btn btn-success">Add</button>
						</div>
					</div>
				</fieldset>
		);
	}
}

const mapStateToProps = (store) => {
	return ({
		hobbiesList: store.hobbiesReducer.hobbiesList
	});
}

const mapDispatchToProps = (dispatch) => {
	return ({
		dispatch,
	});
}
export default connect(mapStateToProps, mapDispatchToProps)(Form); 


// hobi ekleme sayfası yapacagız birde hadi bakalım neyse benzer mantık olacak direk 
