import React from 'react';
// import { useDispatch } from 'react-redux';

// import {
// 	removeHobbies
// } from 'store/actions/hobbiesActions';

export const ListItem = (props) => {
	// const dispatch = useDispatch();

	const getNameFirstChar = (name) => {
		return name.slice(0, 1);
	}
	const removeHobbiesHandler = (id) => {
		// dispatch(removeHobbies(id));
		props.removeHobbies(id);
	}

	return (
		<div className="list-item" data-id={props.id}>
			<div><a href="#" data-abc="true"><span className="w-48 avatar gd-primary">{getNameFirstChar(props.name)}</span></a></div>
			<div className="flex"> <a href="#" className="item-author text-color" data-abc="true">{props.name}</a>
			</div>
			<div className="no-wrap">
				<div className="item-date text-muted text-sm d-none d-md-block">{props.type}</div>
			</div>
			<div className="no-wrap">
				<div className="item-date text-muted text-sm d-none d-md-block">
					{/* <button onClick={() => removeHobbiesHandler(props.id)} className="btn btn-small btn-danger border50"> - </button> */}
				</div>
			</div>
		</div>
	);
}




export default ListItem;
