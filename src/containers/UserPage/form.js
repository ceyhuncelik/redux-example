
import React from 'react';
import { connect } from 'react-redux';
import './style.css';

import UserListItem from 'components/UserListItem';
import Input from 'components/Input';
import Select from 'components/Select';

import {
	saveUsers,
} from 'store/actions/usersActions';

class Form extends React.Component {
	state = {
		name: '',
		age: null,
		gender: null,
	};

	userListRender = userList => userList.map(item => (<UserListItem {...item} />))

	saveUsersHandler = () => {
		const { name, age, gender } = this.state;		
		this.props.dispatch(saveUsers({ name, age, gender }));
	}

	onChangeHandler = (event) => {
		const { value, name } = event.target;
		this.setState({ [name]: value });
	}
	
	render() {
		console.log('Form Render');
		return (
			<fieldset>
					<div id="legend">
						<legend className="">Add New User </legend>
					</div>
					<Input onChange={this.onChangeHandler} label='Name' name='name' />
					<Input onChange={this.onChangeHandler} label='Age' name='age' />
					<Select onChange={this.onChangeHandler} label='Gender' name='gender' options={[{value: 'male', label: 'Male'}, {value: 'female', label: 'Female'}, ]} />
					<div className="control-group">
						<div className="controls">
							<button onClick={() => this.saveUsersHandler() } className="btn btn-success">Add</button>
						</div>
					</div>
				</fieldset>
		);
	}
}

const mapStateToProps = (store) => {
	return ({
		userList: store.usersReducer.userList
	});
}

const mapDispatchToProps = (dispatch) => {
	return ({
		dispatch,
	});
}
export default connect(mapStateToProps, mapDispatchToProps)(Form); 

