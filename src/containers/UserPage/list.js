import React from 'react';
import { connect } from 'react-redux';
import './style.css';

import store from 'store';

import ListItem from './listItem';

import {
	saveUsers,
	// saveUsersNotRec,
} from 'store/actions/usersActions';

class List extends React.Component {

	userListRender = userList => userList.map(item => (<ListItem {...item} removeHobies={this.props.removeHobies} />))


	isReduxStoreReallyImmutable = () => {
		const tempUserList = this.props.userList;
		tempUserList.push({ id: 10, name: 'DENEME', age: 12 });

		console.log(tempUserList, 'local');
		console.log(this.props.userList, 'props');
		console.log(store.getState().usersReducer.userList, 'store');
		// this.forceUpdate();
	
	}

	render() {
		console.log('List render');
		// debugger;
		return (
				<fieldset>
						<div id="legend">
							<legend className="">Users List</legend>
						</div>
				<div className="list list-row block">
					{this.userListRender(this.props.userList)}
					<button onClick={this.isReduxStoreReallyImmutable} > deneme </button>
				</div>
			</fieldset>
		);
	}
}

const mapStateToProps = (store) => {
	return ({
		userList: store.usersReducer.userList
	});
}

const mapDispatchToProps = (dispatch) => {
	return ({
		dispatch,
	});
}
export default connect(mapStateToProps, mapDispatchToProps)(List); 
