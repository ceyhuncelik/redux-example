import React from 'react';
// import { connect } from 'react-redux';
import './style.css';

import List from './list';
import Form from './form';

class UserPage extends React.Component {

	// userListRender = userList => userList.map(item => (<UserListItem {...item} />))

	// saveUsersHandler = () => {
	// 	this.props.dispatch(saveUsers({ id: 3, name: 'Ceyhun ÇELİK', age: 25 }));
	// }

	render() {
		return (
			
			<div className="page-content page-container container" id="page-content">
				
				<div className="padding">
					<div className="row" style={{ display: 'flex'}}>
						<div className="col-sm-6" style={{padding: '0px 15px'}}>
							<List />
						</div>
						<div className="col-sm-6" style={{padding: '0px 15px'}}>
							<Form />
						</div>
					</div>
				</div>
			</div>
		);
	}
}

export default UserPage;

// const mapStateToProps = (store) => {
// 	return ({
// 		userList: store.usersReducer.userList
// 	});
// }

// const mapDispatchToProps = (dispatch) => {
// 	return ({
// 		dispatch,
// 	});
// }
// export default connect(mapStateToProps, mapDispatchToProps)(UserPage);


// aslında arkadas oldukca muthis bi modulerlik katıyor ise redux 
// kullanmak gerek 
// bu akısı kolaylamak adına su bizim daha onceden kullandıgımızıda orneklemek gerek 


// birde table ornegi falan cıkarmak gerek aslında ama bilemedim arkadas

// direk olarak redux tan beslenen bi react table 
// sadece bi key verilecek birde column degerleri o kadarcık 

// on rowClick ine kadar hepsi storeda olacak next page falan filanda dahil tıkladıgında kendi icinde o veri cekme metodunu cagıracak 
// server side tamamen hadi buyrun arkadas 
// super olmaz mı sadece key ve column degelerini vererek kullanım ornegi
// denenebilir acıkcası 



// tamam abi modulerlige de bir ornek gostermis olduk sanırsam bu ornegi bi adım daha oteye tasımak gerek sanırsam 
// yapılacak is nedir burda 
// sayfaları alp farklı bir sayfada mdal icinde acabilirz 

// tablo ile bi sayfa ornegi daha cıkarmak gerek diye dusuuyorum 

// tamam tasıma iside 
// baska ne gibi islerimiz olacak arkadas 
