import React from 'react';
import { connect } from 'react-redux';
import './style.css';

import ListItem from './listItem';

import {
	saveHobbies,
} from 'store/actions/hobbiesActions';

class List extends React.Component {

	hobbiesListRender = hobbiesList => hobbiesList.map(item => (<ListItem {...item} removeHobbies={this.props.removeHobbies} />))

	render() {
		console.log('List render');
		
		return (
			<fieldset>
				<div id="legend">
					<legend className="">Hobbies List</legend>
				</div>
				<div className="list list-row block">
					{this.hobbiesListRender(this.props.hobbiesList)}
					{/* <button onClick={this.storeObjectControl} > deneme </button> */}
				</div>
			</fieldset>
		);
	}
}

export default List; 

// const mapStateToProps = (store) => {
// 	return ({
// 		hobbiesList: store.hobbiesReducer.hobbiesList
// 	});
// }

// const mapDispatchToProps = (dispatch) => {
// 	return ({
// 		dispatch,
// 	});
// }

// export default connect(mapStateToProps, mapDispatchToProps)(List); 
