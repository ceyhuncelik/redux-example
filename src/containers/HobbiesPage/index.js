import React from 'react';
// import { connect } from 'react-redux';
import './style.css';

import List from './list';
import Form from './form';
import LastAddedHobby from './LastAddedHobby';

class HobbiesPage extends React.Component {

	// userListRender = userList => userList.map(item => (<UserListItem {...item} />))

	// saveUsersHandler = () => {
	// 	this.props.dispatch(saveUsers({ id: 3, name: 'Ceyhun ÇELİK', age: 25 }));
	// }
	state = {
		hobbiesList: [
			{ name: 'Reading', type: "literature", id: 1 },
			{ name: 'Walking', type: "sport", id: 2 },
		],
		lastAddedHobbyShow: false,
	};
	
	saveHobbies = (hobby) => {
		const newHobbiesList = this.state.hobbiesList;
		
		// like action operations ;
		let nextId = 0;
		if (newHobbiesList.length > 0) {
			nextId = newHobbiesList[newHobbiesList.length - 1].id + 1;
		}
		newHobbiesList.push({ ...hobby, id: nextId });
		
		// like reduce to state
		this.setState({ hobbiesList: newHobbiesList, lastAddedHobbyShow: true });
	}
	
	removeHobbies = (id) => {
		// like action
		const tempHobbiesList = this.state.hobbiesList;
		const newHobbiesList = tempHobbiesList.filter(item => item.id !== id);
		// like reduce 
		this.setState({ hobbiesList: newHobbiesList });
	}
	close = () => {
		this.setState({ lastAddedHobbyShow: false });
	}
	
	render() {
		const { lastAddedHobbyShow, hobbiesList } = this.state;
		return (
			<div className="page-content page-container container" id="page-content">
				<LastAddedHobby
					hobbiesList={hobbiesList}
					close={this.close}
					removeHobbies={this.removeHobbies}
					lastAddedHobbyShow={lastAddedHobbyShow}
				/>
				<div className="padding">
					<div className="row" style={{ display: 'flex'}}>
						<div className="col-sm-6" style={{padding: '0px 15px'}}>
							<List removeHobbies={this.removeHobbies} hobbiesList={hobbiesList} />
						</div>
						<div className="col-sm-6" style={{padding: '0px 15px'}}>
							<Form saveHobbies={this.saveHobbies}/>
						</div>
					</div>
				</div>
			</div>
		);
	}
}

export default HobbiesPage;

// const mapStateToProps = (store) => {
// 	return ({
// 		userList: store.usersReducer.userList
// 	});
// }

// const mapDispatchToProps = (dispatch) => {
// 	return ({
// 		dispatch,
// 	});
// }
// export default connect(mapStateToProps, mapDispatchToProps)(UserPage);

// last added componenti eklenecek 
// butun akıs state icinden yonetilecek gelistrme suresi uzerine yorum yapılacak 

