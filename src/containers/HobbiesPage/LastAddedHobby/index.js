import React from 'react';
// import { connect } from 'react-redux';
import Item from './item';

import './style.css';


class LastAddedHobby extends React.Component {
  render() {
    const { hobbiesList, lastAddedHobbyShow, close, removeHobbies } = this.props;

    let lastUser = { name: '' }; 
    if (hobbiesList.length > 0) {
      lastUser = hobbiesList[hobbiesList.length - 1];
    } 

    if (!lastAddedHobbyShow) { return null; }
    return (
      <div className="absolute-right">
        <Item {...lastUser} close={close} removeHobbies={removeHobbies}/>
      </div>
    );
  }
}


export default LastAddedHobby;
// const mapStateToProps = (store) => {
//   	return ({
//   		hobbiesList: store.usersReducer.userList,
//   		lastUserletShow: store.usersReducer.lastUserletShow
//   	});
//   }
//   const mapDispatchToProps = (dispatch) => {
//   	return ({
//   		dispatch,
//   	});
//   }
// export default connect(mapStateToProps, mapDispatchToProps)(LastAddedHobby);
  