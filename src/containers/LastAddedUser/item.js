import React from 'react';
import { useDispatch } from 'react-redux';

import {
  removeUsers,
  closeLastAdded,
} from 'store/actions/usersActions';

export const ListItem = (props) => {
	const dispatch = useDispatch();

	const getNameFirstChar = (name) => {
		return name.slice(0, 1);
	}
	const removeUsersHandler = (id) => {
		dispatch(removeUsers(id));
	}
	const close = (id) => {
		// dispatch(closeLastAdded());
		dispatch({ type: 'USERS/CLOSE_LAST_ADDED' });
	}

	return (
		<div className="list-item" data-id={props.id}>
			<div><a href="#" data-abc="true"><span className="w-48 avatar gd-warning">{getNameFirstChar(props.name)}</span></a></div>
			<div className="flex"> <a href="#" className="item-author text-color" data-abc="true">{props.name}</a>
				<div className="item-except text-muted text-sm h-1x">For what reason would it be advisable for me to think about business content?</div>
			</div>
			<div className="no-wrap">
				<div className="item-date text-muted text-sm d-none d-md-block">{props.age}</div>
			</div>
      <br/>
			<div className="no-wrap">
				<div className="item-date text-muted text-sm d-none d-md-block">
					<button onClick={() => removeUsersHandler(props.id)} className="btn btn-small btn-danger "> Remove Last Item </button>
					<button onClick={() => close()} className="btn btn-small btn-danger "> Close </button>
				</div>
			</div>
		</div>
	);
}




export default ListItem;
