import React from 'react';
import { connect } from 'react-redux';
import Item from './item';

import './style.css';


class LastAddedUser extends React.Component {
  render() {
    const { userList, lastUserletShow } = this.props;

    let lastUser = { name: '' }; 
    if (userList.length > 0) {
      lastUser = userList[userList.length - 1];
    } 

    if (!lastUserletShow) { return null; }
    return (
      <div className="absolute-right">
        <Item {...lastUser} />
      </div>
    );
  }
}



const mapStateToProps = (store) => {
  	return ({
  		userList: store.usersReducer.userList,
  		lastUserletShow: store.usersReducer.lastUserletShow
  	});
  }
  const mapDispatchToProps = (dispatch) => {
  	return ({
  		dispatch,
  	});
  }
export default connect(mapStateToProps, mapDispatchToProps)(LastAddedUser);
  