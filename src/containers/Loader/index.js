import React from 'react';
import { useSelector } from 'react-redux';
import './style.css';




const Loader = (props) => {
  const loader = useSelector(store => store.loggerReducer.loader);

  if (!loader) { return null; }
  return (
    <div class="loader-wrapper">
      <svg class="loader" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 340 340">
        <circle cx="170" cy="170" r="160" stroke="#f4c414"/>
        <circle cx="170" cy="170" r="135" stroke="#f45414"/>
        <circle cx="170" cy="170" r="110" stroke="#f4c414"/>
        <circle cx="170" cy="170" r="85" stroke="#f45414"/>
      </svg>
      
    </div>
  )
}


export default Loader;
