import React from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom'

import UserPage from 'containers/UserPage';
import HobbiesPage from 'containers/HobbiesPage';
import UserAndHobbieTogether from 'containers/UserAndHobbieTogether';
import UserPageWithApi from 'containers/UserPageWithApi';
import HobbiesWithApi from 'containers/HobbiesWithApi';
import Header from 'components/Header';
import LastAddedUser from 'containers/LastAddedUser';
import Loader from 'containers/Loader';
import MidlewareAlert from 'components/MidlewareAlert';

class RouterContainer extends React.Component {
  render() {
		// debugger;
		return (
			<>
				<Router>
					<Header />
					<Loader />
					<MidlewareAlert />
					<LastAddedUser />
					<Route path="/users" component={UserPage} />
					<Route path="/hobbies" component={HobbiesPage} />
					<Route path="/together" component={UserAndHobbieTogether} />
					<Route path="/hobies-api" component={HobbiesWithApi} />
					<Route path="/users-api" component={UserPageWithApi} />
				</Router>
			</>
		);
	}
}



export default RouterContainer;
