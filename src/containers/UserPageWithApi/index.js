import React from 'react';
// import { connect } from 'react-redux';
import './style.css';

import List from './list';
import Form from './form';

class UserPageWithApi extends React.Component {

	render() {
		return (
			
			<div className="page-content page-container container" id="page-content">
				
				<div className="padding">
					<div className="row" style={{ display: 'flex'}}>
						<div className="col-sm-6" style={{padding: '0px 15px'}}>
							<List />
						</div>
						<div className="col-sm-6" style={{padding: '0px 15px'}}>
							<Form />
						</div>
					</div>
				</div>
			</div>
		);
	}
}

export default UserPageWithApi;


// Servis istegi atılacak 
// farklı ornekler gosterilecek thunk ve saga ile 
// bunları da tamamladıktan sonra 
// sanırsam isimiz tamam belki yarın biseyler ekleriz bilemedim 

// farklılastırabilecegim ne gibi ornekler var bunlarada bi goz atmam gerek arkadas 

// bunun aksiyonları vs farklı olacak 
// apiActions 
// apiReducers 
// api saga
// api thunk 
// ayrıstırmak gerek anladıgım kadaryla 

// iki farklı ornek farklılıkları neler ne kullanabilir hangisi daha islevsel gorunuyor gibi eklemeler 
// yapılabilir 

// honbies ekranını state e cek reducer dan redux tan kurtar ve servis istegi ornegi yap iki tane 