import React from 'react';
import { connect } from 'react-redux';
import './style.css';

import store from 'store';

import ListItem from './listItem';

import {
	saveUsers,
	// saveUsersNotRec,
} from 'store/actions/usersActions';
import { getUsers } from 'store/actions/userApiActions';

class List extends React.Component {
	constructor(props) {
		super(props);
		this.props.dispatch(getUsers());
		
	}
	userListRender = userList => userList.map(item => (<ListItem {...item} />))

	saveUsersHandler = () => {
	}

	render() {
		return (
				<fieldset>
						<div id="legend">
							<legend className="">Users List With Api</legend>
						</div>
				<div className="list list-row block">
					{this.userListRender(this.props.userList || [])}
					{/* <button onClick={this.storeObjectControl} > deneme </button> */}
				</div>
			</fieldset>
		);
	}
}

const mapStateToProps = (store) => {
	return ({
		userList: store.userApiReducer.userList
	});
}

const mapDispatchToProps = (dispatch) => {
	return ({
		dispatch,
	});
}
export default connect(mapStateToProps, mapDispatchToProps)(List); 
