import React from 'react';

import UserPage from 'containers/UserPage';
import HobbiesPage from 'containers/HobbiesPage';

const UserAndHobbieTogether = () => {


  return (
    <>
      <UserPage />
      <HobbiesPage />
    </>
  );
}


export default UserAndHobbieTogether;

