

export const getApiCall = (url) => {
  return fetch(url)
  .then((res) => {
    return res.json();
  })
}