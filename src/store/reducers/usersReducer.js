import {
  SAVE_USERS,
	REMOVE_USERS,
	CLOSE_LAST_ADDED,

  SAVE_USERS_NOT_REC,
  REMOVE_USERS_NOT_REC,
} from './../actionTypes';

const initialStore = {
	userList: [
		{ name: 'Sam Duran', id: 1, age: '25' },
		{ name: 'Jonh Hery', id: 2, age: '19' },
	],
	lastUserletShow: false,
};

const usersReducers = (store = initialStore, action) => {
	// debugger;
	switch (action.type) {
		case SAVE_USERS_NOT_REC: {
			const nextStore = store;
			nextStore.userList.push(action.userData);
			return nextStore;
		}
		// !!
		case SAVE_USERS: {
			const userList = action.newUserList;
			return { 
				...store, 
				userList,
				lastUserletShow: true,
			};
		}
		// -- 
		case CLOSE_LAST_ADDED: {
			return { 
				...store, 
				lastUserletShow: false,
			};
		}
		// -------------------------------------------------------

		case REMOVE_USERS_NOT_REC: {
			const nextStore = store;
			nextStore.userList = store.userList.filter(item => item !== action.userData.id);
			return nextStore;
		}

		case REMOVE_USERS: {
			const userList = action.newUserList;
			return { 
				...store,
				userList,
			};
		}
			
		default:
			return store;
	}
}

export default usersReducers;

