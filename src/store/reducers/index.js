import { combineReducers } from 'redux';
import usersReducer from './usersReducer';
import hobbiesReducer from './hobbiesReducer';
import userApiReducer from './userApiReducer';
import loggerReducer from './loggerReducer';
// import counter from './counter'

export default combineReducers({  
	usersReducer,
	hobbiesReducer,
	userApiReducer,
	loggerReducer,
});