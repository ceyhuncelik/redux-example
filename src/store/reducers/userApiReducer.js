import {
  GET_USERS,
  GET_USERS_SUCCESS,
  GET_USERS_FAILURE,
} from 'store/actionTypes';

const initialStore = {
  userList: [],
  loader: false,
};

const userApiReducer = (store = initialStore, action) => {
	switch (action.type) {

		case GET_USERS: {
			const { loader } = action;
			return { ...store, loader };
    }
    
		case GET_USERS_SUCCESS: {
      const { userList, loader } = action;
			return { ...store, userList, loader };
    }
    
		case GET_USERS_FAILURE: {
      const { error, loader } = action.error;
			return { ...store, error, loader };
		}
			
		default:
			return store;
	}
}

export default userApiReducer;

