import {
  SAVE_HOBBIES,
  REMOVE_HOBBIES, 
} from './../actionTypes';

const initialStore = {
	hobbiesList: [
		{ name: 'Reading', type: "literature" },
		{ name: 'Walking', type: "sport" },
	],
};

const hobbiesReducers = (store = initialStore, action) => {
	switch (action.type) {

		case SAVE_HOBBIES: {
			const hobbiesList = action.newHobbiesList;
			return { ...store, hobbiesList};
    }
    
		case REMOVE_HOBBIES: {
			const hobbiesList = action.newHobbiesList;
			return { ...store, hobbiesList};
		}
			
		default:
			return store;
	}
}

export default hobbiesReducers;

