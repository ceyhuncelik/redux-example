import { 
  LOGGER_OPEN,
  LOGGER_CLOSE,
  
  LOADER_OPEN,
  LOADER_CLOSE,
} from '../actionTypes';


const initialStore = {
  isOpen: true,
  message: null,
  loader: false,
};

const hobbiesReducers = (store = initialStore, action) => {
	switch (action.type) {

		case LOGGER_OPEN: {
			const { message } = action;
			return { ...store, isOpen: true, message };
    }
    
		case LOGGER_CLOSE: {
      return { ...store, isOpen: false, message: null };
		}
			
		case LOADER_OPEN: {
			return { ...store, loader: true };
    }
    
		case LOADER_CLOSE: {
      return { ...store, loader: false };
		}
			
		default:
			return store;
	}
}

export default hobbiesReducers;

