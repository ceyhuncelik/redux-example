export const SAVE_USERS = 'USERS/SAVE_USERS';
export const REMOVE_USERS = 'USERS/REMOVE_USERS';

export const UPDATE_USERS = 'USERS/UPDATE_USERS';

export const SAVE_USERS_NOT_REC = 'USERS/SAVE_USERS_NOT_REC';
export const UPDATE_USERS_NOT_REC = 'USERS/UPDATE_USERS_NOT_REC';
export const REMOVE_USERS_NOT_REC = 'USERS/REMOVE_USERS_NOT_REC';
export const CLOSE_LAST_ADDED = 'USERS/CLOSE_LAST_ADDED';


export const SAVE_HOBBIES = 'HOBBIES/SAVE_HOBBIES';
export const REMOVE_HOBBIES = 'HOBBIES/REMOVE_HOBBIES';

export const GET_USERS = 'USER_API/GET_USERS';
export const GET_USERS_SUCCESS = 'USER_API/GET_USERS_SUCCESS';
export const GET_USERS_FAILURE = 'USER_API/GET_USERS_FAILURE';

export const LOGGER_OPEN = 'LOGGER/LOGGER_OPEN';
export const LOGGER_CLOSE = 'LOGGER/LOGGER_CLOSE';

export const LOADER_OPEN = 'LOGGER/LOADER_OPEN';
export const LOADER_CLOSE = 'LOGGER/LOADER_CLOSE';

