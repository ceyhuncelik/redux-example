import store from './index';




export const userListSelector = () => {
  return store.getState().usersReducer.userList;
}

export const hobbiesListSelector = () => {
  return store.getState().hobbiesReducer.hobbiesList;
}
