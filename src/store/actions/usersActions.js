import {
  SAVE_USERS,
	REMOVE_USERS,
	CLOSE_LAST_ADDED,

	SAVE_USERS_NOT_REC,
	REMOVE_USERS_NOT_REC,
} from 'store/actionTypes';

import { userListSelector } from 'store/selectors';

// !! 
export const saveUsersNotRec = (userData) =>{
	return {
		type: SAVE_USERS_NOT_REC,
		userData,
	}
}

export const closeLastAdded = (userData) =>{
	return {
		type: CLOSE_LAST_ADDED,
	}
}

export const saveUsers = (userData) =>{
	const newUserList = JSON.parse(JSON.stringify(userListSelector()));
	// debugger;
	let nextId = 0;
	if (newUserList.length > 0) {
		nextId = newUserList[newUserList.length - 1].id + 1;
	}
	newUserList.push({ ...userData, id: nextId });
	
	return {
		type: SAVE_USERS,
		newUserList,
		data: userData,
	}
}

// Remove User action examples ---------------------------------------------------------

// !!
export const removeUserNotRec = (userData) =>{
	return {
		type: REMOVE_USERS,
		userData,
	}
}

export const removeUsers = (id) =>{
	
	const tempUserList = JSON.parse(JSON.stringify(userListSelector()));
	const newUserList = tempUserList.filter(item => item.id !== id);
	
	return {
		type: REMOVE_USERS,
		newUserList,
		data: id,
	}
}