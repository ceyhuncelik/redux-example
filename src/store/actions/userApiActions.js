import {
  GET_USERS,
  GET_USERS_SUCCESS,
  GET_USERS_FAILURE,
} from 'store/actionTypes';


export const getUsers = () => {
  // servis istegi atılacak loader ative edilecek 
  return {
    type: GET_USERS,
    loader: true,
  }  
}

export const getUsersSuccess = (result) => {
  return {
    type: GET_USERS_SUCCESS,
    loader: false,
    userList: result,
  }  
}

export const getUsersFailure = (error) => {
  return {
    type: GET_USERS_FAILURE,
    loader: false,
    error
  }  
}