import { 
  LOGGER_OPEN,
  LOGGER_CLOSE,

  LOADER_OPEN,
  LOADER_CLOSE,
} from '../actionTypes';


export const loggerOpen = (message) => ({
  type: LOGGER_OPEN,
  message,
})
export const loggerClose = () => ({
  type: LOGGER_CLOSE,
})
export const loaderOpen = (message) => ({
  type: LOADER_OPEN,
})
export const loaderClose = () => ({
  type: LOADER_CLOSE,
})