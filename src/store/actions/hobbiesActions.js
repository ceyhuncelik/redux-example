import {
  SAVE_HOBBIES,
  REMOVE_HOBBIES
} from 'store/actionTypes';

import { hobbiesListSelector } from 'store/selectors';


export const saveHobbies = (hobbiesData) =>{
	const newHobbiesList = JSON.parse(JSON.stringify(hobbiesListSelector()));
	// debugger;
	let nextId = 0;
	if (newHobbiesList.length > 0) {
		nextId = newHobbiesList[newHobbiesList.length - 1].id + 1;
	}
	newHobbiesList.push({ ...hobbiesData, id: nextId });
	
	return {
		type: SAVE_HOBBIES,
		newHobbiesList,
		data: hobbiesData,
	}
}

// Remove Hobbies action examples ---------------------------------------------------------

export const removeHobbies = (id) =>{
	
	const tempUserList = JSON.parse(JSON.stringify(hobbiesListSelector()));
	const newHobbiesList = tempUserList.filter(item => item.id !== id);
	
	return {
		type: REMOVE_HOBBIES,
		newHobbiesList,
		data: id,
	}
}