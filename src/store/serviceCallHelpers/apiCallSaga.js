import { call, put, takeEvery, takeLatest, delay } from 'redux-saga/effects'
import { getApiCall } from 'utils/api';
import { GET_USERS } from 'store/actionTypes';
import { 
  getUsersSuccess,
  getUsersFailure
} from 'store/actions/userApiActions';


function* getUsersHandler(action) {
   try {
      const result = yield call(getApiCall, 'http://localhost:3001/userList');
      // debugger;
			yield delay(1000);
      yield put(getUsersSuccess(result));
		} catch (err) {
			yield delay(1000);
      yield put(getUsersFailure(err));
   }
}

export default function* apiCallSaga() {
  yield takeLatest(GET_USERS, getUsersHandler);
}
