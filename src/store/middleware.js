import { 
  
} from './actionTypes';

import { loggerOpen, loaderOpen, loaderClose } from './actions/loggerActions'
const customMiddleWare = store => next => action => {
  console.log("Middleware triggered:", action);
  if (action.loader) {
    next(loaderOpen());
  } else {
    next(loaderClose());
  }
  const message = generateMessage(action);
  next(loggerOpen(message));
  next(action);
}

export default customMiddleWare;


// hepsi icin bi generator uretmemiz gerek arkadas 


const generateMessage = (action) => {
  const actionTypePrefix = action.type.split('/');
  switch (actionTypePrefix[0]) {
    case 'USERS':
      if (actionTypePrefix[1].indexOf('SAVE') !== -1) {
        return `${action.data.name} added to user list userGender: ${action.data.gender.value}`;
      } else if (actionTypePrefix[1].indexOf('REMOVE') !== -1) {
        return `Id: ${action.data} removed to user list`;
      }
      return `${action.type} action called`;

    case 'HOBBIES':
      if (actionTypePrefix[1].indexOf('SAVE') !== -1) {
        return `${action.data.name} added to hobbies list type: ${action.data.type}`;
      } else if (actionTypePrefix[1].indexOf('REMOVE') !== -1) {
        return `Id: ${action.data} removed to hobbies list`;
      }
      return `${action.type} action called`;
  
    default:
      return `${action.type} action called`;
  }
}