
import { createStore, applyMiddleware } from "redux";
import reducers from "./reducers";
import { composeWithDevTools } from 'redux-devtools-extension';
import createSagaMiddleware from 'redux-saga'



import apiCallSaga from './serviceCallHelpers/apiCallSaga'
import customMiddleWare from "./middleware";

const sagaMiddleware = createSagaMiddleware()



const store = createStore(reducers, composeWithDevTools(
  applyMiddleware(sagaMiddleware, customMiddleWare),
  // other store enhancers if any
));

sagaMiddleware.run(apiCallSaga);

export default store;
