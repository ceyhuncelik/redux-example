import React, { useState, useEffect } from 'react';
import { useSelector } from 'react-redux';
import './style.css';

export const MidlewareAlert = (props) => {
  const loggerReducer = useSelector(store => store.loggerReducer);
  
  const [isOpen, setIsOpen] = useState(false);
  const className = ['midleware-alert-wrapper'];
  className.push(props.className);
  if (isOpen) {
    className.push('open');
  }
  useEffect(() => {
    setIsOpen(loggerReducer.isOpen);
  }, [loggerReducer])
	return (
    <>
		<div className={className.join(' ')}>
      {loggerReducer.message}      
      <span className="close-icon" onClick={() => setIsOpen(!isOpen)}> &times; </span>
    </div>
    </>
	);
}

export default MidlewareAlert;
