import React from 'react';
import { Link } from 'react-router-dom';
import './style.css';


const Header = () => {
  return(
    <div className="header-wrapper">
			<Link className="link" to="/users">User Page</Link>
			<Link className="link" to="/hobbies">Hobies Page</Link>
			<Link className="link" to="/together">Hobies and User Page Together</Link>
			<Link className="link" to="/users-api">User Page With Api</Link>
			<Link className="link" to="/hobies-api">hb Page With Api</Link>
		</div>
  );
}


export default Header;