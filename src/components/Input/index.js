import React from 'react';


export const Input = (props) => {

	return (
		<div className="control-group">
			<label className="control-label" for="username">{props.label}</label>
			<div className="controls">
				<input onChange={props.onChange} type={props.type} id={props.id} name={props.name} placeholder="" className="input-xlarge" />
			</div>
		</div>
	);
}




export default Input;

