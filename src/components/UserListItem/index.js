import React from 'react';


export const UserListItem = (props) => {

	const getNameFirstChar = (name) => {
		return name.slice(0, 1);
	}
	
	return (
		<div className="list-item" data-id={props.id}>
			<div><a href="#" data-abc="true"><span className="w-48 avatar gd-warning">{getNameFirstChar(props.name)}</span></a></div>
			<div className="flex"> <a href="#" className="item-author text-color" data-abc="true">{props.name}</a>
				<div className="item-except text-muted text-sm h-1x">For what reason would it be advisable for me to think about business content?</div>
			</div>
			<div className="no-wrap">
				<div className="item-date text-muted text-sm d-none d-md-block">{props.age}</div>
			</div>
		</div>
	);
}




export default UserListItem;

// id name age 
// bu arkadas direk olarak selector ile store a baglayablir miyiz dizi elemanının 
// baglamak icin sanrısam bilemedim ki arkadas 

// sanırsam bunu degilde bir tane son eklenen diye ayırıp onu takip ettirebiliriz direk 
// olarak ne bileyim bri tane card last user card ı yapabiliriz sanırasm hadi bakalım 
// unutmadan bootstrap gerekli 
