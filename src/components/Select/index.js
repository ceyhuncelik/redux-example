import React from 'react';
import ReactSelect from 'react-select'

export const Select = (props) => {


  const onChange = (value) => {
    const eventProxy = {
      target: {
        name: props.name,
        value: value
      },
    };
    props.onChange(eventProxy);
  }
	return (
		<div className="control-group">
			<label className="control-label" for="username">{props.label}</label>
			<div className="controls">
        <ReactSelect
          onChange={onChange}
          options={props.options}
          type={props.type}
          id={props.id}
          name={props.name}
          placeholder={props.placeHolder}
          className="input-xlarge"
         />
			</div>
		</div>
	);
}




export default Select;
