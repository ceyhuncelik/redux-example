import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import './index.css';
import App from './App';
import RouterContainer from 'containers/RouterContainer';
import * as serviceWorker from './serviceWorker';

import store from './store';

const Root = () => {
  // debugger;
  return (
    <React.StrictMode>
      <Provider store={store}>
        <RouterContainer />
      </Provider>
    </React.StrictMode>
  );
}

ReactDOM.render(
  <Root />,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
